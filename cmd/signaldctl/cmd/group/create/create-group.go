// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package create

import (
	"encoding/json"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/cmd/signaldctl/config"
	"gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

var (
	account        string
	adminMembers   bool
	timer          int64
	avatar         string
	CreateGroupCmd = &cobra.Command{
		Use:   "create <group title> [<member number or UUID> [<member number or UUID>...]]",
		Short: "create a group",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) == 0 {
				common.Must(cmd.Help())
				log.Fatal("must at least specify a group title")
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)
			req := v1.CreateGroupRequest{
				Account: account,
				Title:   args[0],
			}

			for _, member := range args[1:] {
				address, err := common.StringToAddress(member)
				if err != nil {
					log.Fatal(err)
				}
				req.Members = append(req.Members, &address)
			}

			if adminMembers {
				req.MemberRole = "ADMINISTRATOR"
			}

			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err, "error communicating with signald")
			}
			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"ID", "Title", "Members"})

				t.AppendRow(table.Row{resp.ID, resp.Title, len(resp.Members)})

				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	CreateGroupCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
	CreateGroupCmd.Flags().BoolVar(&adminMembers, "admin-members", false, "grants all members of the new group administrator permissions. otherwise only the group creator will be a group administrator.")
	CreateGroupCmd.Flags().Int64VarP(&timer, "timer", "t", 0, "sets the disappearing message timer of the new group")
	CreateGroupCmd.Flags().StringVar(&avatar, "avatar", "", "path to the image to use as the group avatar")
}
