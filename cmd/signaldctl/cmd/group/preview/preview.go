// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package preview

import (
	"encoding/json"
	"errors"
	"log"
	"os"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/cmd/signaldctl/config"
	"gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

var (
	account         string
	joinURL         string
	PreviewGroupCmd = &cobra.Command{
		Use:   "preview https://signal.group/...",
		Short: "preview information about a group without joining",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}

			if len(args) == 0 {
				return errors.New("please specify signald.group URL")
			}
			joinURL = args[0]

			if !strings.HasPrefix(joinURL, "https://signal.group/#") {
				return errors.New("invalid group link (must start with https://signal.group/#")
			}

			return nil
		},
		RunE: func(_ *cobra.Command, args []string) error {
			go common.Signald.Listen(nil)
			req := v1.GroupLinkInfoRequest{
				Account: account,
				Uri:     joinURL,
			}

			info, err := req.Submit(common.Signald)
			if err != nil {
				return err
			}

			switch common.OutputFormat {
			case common.OutputFormatJSON:
				return json.NewEncoder(os.Stdout).Encode(info)
			case common.OutputFormatYAML:
				return yaml.NewEncoder(os.Stdout).Encode(info)
			case common.OutputFormatDefault, common.OutputFormatTable:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)

				joinApproval := "unknown"
				switch info.AddFromInviteLink {
				case 3:
					joinApproval = "yes"
				case 1:
					joinApproval = "no"
				}

				t.AppendRows([]table.Row{
					table.Row{"Title", info.Title},
					table.Row{"Group ID", info.GroupID},
					table.Row{"Member Count", info.MemberCount},
					table.Row{"Membership Approval", joinApproval},
				})
				common.StylizeTable(t)
				t.Render()
			}

			return nil
		},
	}
)
