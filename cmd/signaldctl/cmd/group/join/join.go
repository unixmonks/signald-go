// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package join

import (
	"encoding/json"
	"log"
	"os"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/cmd/signaldctl/config"
	"gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

var (
	account string
	url     string

	JoinGroupCmd = &cobra.Command{
		Use:   "join <url>",
		Short: "join a group by URL",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) < 1 {
				common.Must(cmd.Help())
				log.Fatal("must specify group url")
			}
			url = args[0]
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)
			req := v1.JoinGroupRequest{
				Account: account,
				Uri:     url,
			}

			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err, "error communicating with signald")
			}

			if resp.PendingAdminApproval {
				log.Println("requested to join pending admin approval")
			} else {
				log.Println("joined group")
			}

			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendRows([]table.Row{
					{"Group ID", resp.GroupID},
					{"Revision", resp.Revision},
					{"Title", resp.Title},
					{"Description", resp.Description},
					{"Members", resp.MemberCount},
					{"Awaiting approval", resp.PendingAdminApproval},
				})

				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			case common.OutputFormatQuiet, common.OutputFormatDefault:
				return
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	JoinGroupCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
