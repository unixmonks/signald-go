package requestsync

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/cmd/signaldctl/config"
	v1 "gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

var (
	account        string
	RequestSyncCmd = &cobra.Command{
		Use:   "request-sync",
		Short: "Ask other devices on the account to send sync data. Must subscribe for result",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)
			req := &v1.RequestSyncRequest{Account: account}
			err := req.Submit(common.Signald)
			if err != nil {
				panic(err)
			}
			log.Println("sync requested. Must be subscribed to receive response")
		},
	}
)

func init() {
	RequestSyncCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
