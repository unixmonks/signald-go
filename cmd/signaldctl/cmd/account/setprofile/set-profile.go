// Copyright © 2021 The signald-go Contributors.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package setprofile

import (
	"log"

	"github.com/spf13/cobra"

	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/cmd/signaldctl/config"
	"gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

var (
	account string
	name    string
	avatar  string
	emoji   string
	about   string

	SetProfileCmd = &cobra.Command{
		Use:   "set-profile [name]",
		Short: "update an account's profile data",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}

			if len(args) > 0 {
				name = args[0]
			}
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)
			req := v1.SetProfile{
				Account:    account,
				Name:       name,
				AvatarFile: avatar,
				Emoji:      emoji,
				About:      about,
			}
			err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal("error from signald: ", err)
			}
			log.Println("profile set")
		},
	}
)

func init() {
	SetProfileCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
	SetProfileCmd.Flags().StringVarP(&avatar, "avatar", "A", "", "path to avatar file")
	SetProfileCmd.Flags().StringVar(&emoji, "emoji", "", "an emoji to be shown next to the about section")
	SetProfileCmd.Flags().StringVar(&about, "about", "", "profile about section")
}
