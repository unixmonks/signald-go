// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package config

import (
	"log"
	"os"
	"path"

	"gopkg.in/yaml.v2"
)

// Configuration is the signaldctl configuration
// remember to update cmd/signaldctl/cmd/config/set when adding here
type Configuration struct {
	// DefaultAccount will be used if --account is not provided
	DefaultAccount string

	// SocketPath is the path to signald.sock
	SocketPath string
}

var Path string

// Config is the variable the configuration gets loaded into
// values shown here are used as defaults
var Config = Configuration{}

func Load() error {
	f, err := os.Open(Path)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}
	defer f.Close()
	if err := yaml.NewDecoder(f).Decode(&Config); err != nil {
		return err
	}
	return nil
}

func Save() error {
	if err := os.MkdirAll(path.Dir(Path), 0770); err != nil {
		return err
	}
	f, err := os.Create(Path)
	if err != nil {
		return err
	}
	defer f.Close()
	log.Println("saving config")
	return yaml.NewEncoder(f).Encode(Config)
}
